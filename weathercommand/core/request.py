import os
from selenium import webdriver


class Request:

    def __init__(self, base_url):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--window-size=1920x1080')

        self._chrome_path = os.path.join(os.curdir, './weathercommand/chromedriver/chromedriver')
        self._base_url = base_url
        self._driver = webdriver.Chrome(self._chrome_path, chrome_options=chrome_options)

        #self._driver_path = os.path.join(os.curdir, 'phantomjs/bin/phantomjs')
        #self._base_url = base_url

        #self._driver = webdriver.PhantomJS(self._driver_path)

    def fetch_data(self, forecast, area):
        url = self._base_url.format(forecast=forecast, area=area)
        self._driver.get(url)

        if self._driver.title  == '404 Not Found':
            error_message = ('Could not find the area that you searching for')

            raise Exception(error_message)

        return self._driver.page_source
